<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Artisan;
use App\Thread;

// spusti scheduler pre queue workera, ktory sa o to postara


class CloseThread implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $threads_to_close;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($threads)
    {
        $this->threads_to_close = $threads;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->threads_to_close->each(function($thread) {
            Thread::where('id', $thread->id)->first()->update(['closed' => true]);
        });

        // foreach ($this->threads_to_close as $thread) {
        //     $thread = Thread::find($thread->id);
        //     $thread->closed = true;
        //     $thread->save();
        // }
        // ($this->threads_to_close)->each(function($thread){
        //     $thread = Thread::find($thread->id);
        //     $thread->closed = true;
        //     $thread->save();
        // });
        sleep(3);
    }
}
