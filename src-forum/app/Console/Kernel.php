<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CloseOldThreads::class,
        Commands\queueCloseThreads::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Running a cron JOB:
        //* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
        // that will run scheduler
        // GCM + 2 hours
        $schedule->command('queue:work')->dailyAt('21:53');
        //->hourly()
        //->unlessBetween('23:40', '4:00');
        /*
        update threads set closed = false;
        delete from jobs where id > 1;
        select count(*) from threads where closed = true;
        */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
